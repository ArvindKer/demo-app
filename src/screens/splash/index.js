//TODO:- imports
import React, { Component } from 'react';
import { StatusBar, View, Text } from 'react-native';
import styles from './styles';

//TODO:- splash class
export default class Splash extends Component {

//TODO:- constructor
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    
//TODO:- class life cycle
    componentDidMount() {
        console.disableYellowBox = true;
        var that = this;
        setTimeout(function () {
            that.getInitialRoot("AppDrawer");
        }, 3000);
    }

//TODO:- Other Functions
    getInitialRoot = (screen_to_load) => {
        setTimeout(() => {
            this.props.navigation.navigate(screen_to_load);
        }, 200);
    }

//TODO:- render event
    render() {
        return (
            <View style={styles.main_container} >
                <StatusBar backgroundColor="#fff" barStyle='dark-content' />
                <View style={styles.container}>
                    <Text style={styles.txt_style}> Demo App </Text>
                </View>
            </View>
        );
    }
}
