//Imports
import { StyleSheet } from "react-native";

export default StyleSheet.create({
    main_container: {
        backgroundColor: "#fff", 
        height: '100%'
    },
    container : {
        flex: 1, 
        width: '100%', 
        backgroundColor: "#fff", 
        alignItems: 'center', 
        justifyContent: 'center', 
    },
    txt_style: {
        color: 'grey',
        fontSize: 22,
        fontWeight: '600',
    }
});