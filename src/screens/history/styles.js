//TODO:- imports
import { StyleSheet } from "react-native";

//TODO:- styleSheet
export default StyleSheet.create({
   
    flatlist: {
        width: "100%",
        flex: 1,
        marginTop: 15,
    },
    flatlist_btn: {
        height: 45,
        width: "100%",
        marginTop: 1,
        flexDirection: 'row',
        alignItems: 'center'
        
    },
    flatlist_txt: {
        fontSize: 16,
        marginLeft: 14,
    }
});