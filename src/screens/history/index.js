//TODO:- imports
import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, Alert } from 'react-native';
import AssetsImages from '../../res'
import global_style from '../../global/global_style';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './styles'
import { DeleteAllHistory } from '../../redux/action/history_action'

//TODO:- history class
class History extends Component {

  //TODO:- constructor
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  //TODO:- render event
  render() {
    return (
      <SafeAreaView style={global_style.safearea_container}>

        <View style={global_style.top_navigation_bar_container}>
          <TouchableOpacity style={global_style.menu_btn}
            onPress={() => { this.props.navigation.openDrawer(); }}
          >
            <Image style={global_style.menu_btn_img} source={AssetsImages.icon_menu} />
          </TouchableOpacity>
          <Text style={global_style.top_navigation_bar_txt}>History</Text>

          {
            this.props.history_reducer.history_data.length != 0 ?

              <TouchableOpacity style={global_style.top_navigation_bar_blank_view}
                onPress={() => {
                  Alert.alert(
                    "Delete all history",
                    "Are you sure, you want to delete all your history?",
                    [
                      {
                        text: 'Yes',
                        onPress: () => {
                          this.props.DeleteAllHistory();
                        },
                      },
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                      },
                    ],
                    { cancelable: false }
                  );

                }}
              >
                <Image style={global_style.menu_btn_img} source={AssetsImages.delete_all} />
              </TouchableOpacity>
              :
              <View style={global_style.top_navigation_bar_blank_view} />

          }
        </View>
        {
          this.props.history_reducer.history_data.length == 0 ?
            <View style={global_style.coming_soon_container}>
              <Text style={global_style.coming_soon_txt}> No data in history </Text>
            </View>
            :
            <FlatList
              style={styles.flatlist}
              showsVerticalScrollIndicator={false}
              data={this.props.history_reducer.history_data}
              renderItem={({ item, index }) => {
                return (
                  <View
                    style={[styles.flatlist_btn, { backgroundColor: "#fff" }]}
                  >
                    <Text style={[styles.flatlist_txt, { color: "#BDD63B" }]}> {item.input_data} </Text>
                  </View>
                );
              }}
            />
        }
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return { history_reducer: state.history_reducer, }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    DeleteAllHistory
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(History);
