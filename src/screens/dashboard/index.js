//TODO:- imports
import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, TextInput, Keyboard } from 'react-native';
import AssetsImages from '../../res'
import global_style from '../../global/global_style';
import styles from './styles';
import { AddToHistory } from '../../redux/action/history_action'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//TODO:- dashboard class
class Dashboard extends Component {

  //TODO:- constructor
  constructor(props) {
    super(props);
    this.state = {
      txt_field_data: ""
    };
  }

  //TODO:- render event
  render() {
    return (
      <SafeAreaView style={global_style.safearea_container}>

        <View style={global_style.top_navigation_bar_container}>
          <TouchableOpacity style={global_style.menu_btn}
            onPress={() => { this.props.navigation.openDrawer(); }}
          >
            <Image style={global_style.menu_btn_img} source={AssetsImages.icon_menu} />
          </TouchableOpacity>
          <Text style={global_style.top_navigation_bar_txt}>Dashboard</Text>
          <View style={global_style.top_navigation_bar_blank_view} />
        </View>

        <TextInput
          style={styles.txtinput_txt_style}
          placeholder="Enter text here"
          placeholderTextColor='rgba(255,255,255,0.6)'
          onChangeText={text => {
            this.setState({ txt_field_data: text });
          }}
          maxLength={25}
          value={this.state.txt_field_data}
        />

        <View style={{ flex: 1 }} />
        <TouchableOpacity style={styles.submit_btn}
          onPress={() => {
            if (this.state.txt_field_data != "") {
              data = {
                "txt_entered": this.state.txt_field_data
              }
              this.props.AddToHistory(data);
              this.setState({ txt_field_data: "" }, () => {
                Keyboard.dismiss()
                setTimeout(function () {
                  alert("Text entered to history")
              }, 100);
              })
            }
          }}
        >
          <Text style={styles.submit_btn_txt}>Submit</Text>
        </TouchableOpacity>

      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    AddToHistory
  }, dispatch)
);

export default connect(null, mapDispatchToProps)(Dashboard);

