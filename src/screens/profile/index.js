//TODO:- imports
import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image } from 'react-native';
import AssetsImages from '../../res'
import global_style from '../../global/global_style';

//TODO:- profile class
export default class Profile extends Component {

  //TODO:- constructor
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  //TODO:- render event
  render() {
    return (
      <SafeAreaView style={global_style.safearea_container}>

        <View style={global_style.top_navigation_bar_container}>
          <TouchableOpacity style={global_style.menu_btn}
            onPress={() => { this.props.navigation.openDrawer(); }}
          >
            <Image style={global_style.menu_btn_img} source={AssetsImages.icon_menu} />
          </TouchableOpacity>
          <Text style={global_style.top_navigation_bar_txt}>Profile</Text>
          <View style={global_style.top_navigation_bar_blank_view} />
        </View>

        <View style={global_style.coming_soon_container}>
          <Text style={global_style.coming_soon_txt}> Coming soon </Text>
        </View>

      </SafeAreaView>
    );
  }
}
