import React, { Component } from 'react';
import {Dimensions } from 'react-native';

const base_unit_height = 640;
const base_unit_width = 360;
const heigth = Dimensions.get('window').height
const width = Dimensions.get('window').width

const NormalizeSize = {
  
  normalize_height: (size) => {
      return (size / base_unit_height) * heigth
  },
  
  normalize_width: (size) => {
    return (size / base_unit_width) * width
  }

}

export default NormalizeSize;
